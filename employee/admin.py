from django.contrib import admin
from employee.models import Employee, BankAccount


class EmployeeAdmin(admin.ModelAdmin):
    pass


class BankAccountAdmin(admin.ModelAdmin):
    pass


admin.site.register(Employee, EmployeeAdmin)
admin.site.register(BankAccount, BankAccountAdmin)
