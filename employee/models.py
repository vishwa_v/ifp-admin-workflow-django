from django.db import models
from django.contrib.auth.models import User


class Employee(models.Model):
    emp_id = models.CharField(max_length=20, blank=True, null=True)
    dob = models.DateField(blank=True, null=True)
    doj = models.DateField(blank=True, null=True)
    phone = models.CharField(max_length=25, blank=True, null=True)
    perm_addr = models.CharField(max_length=255, blank=True, null=True)
    comm_addr = models.CharField(max_length=255, blank=True, null=True)
    user = models.OneToOneField(User, on_delete=models.SET_NULL, null=True, blank=True)


class BankAccount(models.Model):
    CURRENT_ACC = "CUR"
    SAVINGS_ACC = "SB"
    SWIFT = "SWIFT"
    IBAN = "IBAN"
    IFSC = "IFSC"
    ACC_TYPE_CHOICES = [
        (CURRENT_ACC, "Current Account"),
        (SAVINGS_ACC, "Savings Account"),
    ]
    UID_TYPE_CHOICES = [
        (IBAN, "IBAN"),
        (IFSC, "IFSC"),
        (SWIFT, "SWIFT"),
    ]
    acc_name = models.CharField(max_length=100, blank=True, null=True)
    acc_no = models.CharField(max_length=40, blank=True, null=True)
    branch = models.CharField(max_length=100, blank=True, null=True)
    uid = models.CharField(max_length=40, blank=True, null=True)
    uid_type = models.CharField(max_length=10, blank=True, null=True)
    acc_type = models.CharField(max_length=10, blank=True, null=True)
    user = models.OneToOneField(User, on_delete=models.SET_NULL, null=True, blank=True)
