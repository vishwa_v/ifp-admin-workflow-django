# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [0.3.0]

- Add employee app
- Add models for employee app

## [0.2.0] - 2022-06-16

### Added

- Add Dockerfile, docker-compose

## [0.1.0] - 2022-05-18

### Added

- Started the a fresh django-project
- Added gitignore, LICENSE, CHANGELOG.md, requirements.txt, dockerignore, VERSION
